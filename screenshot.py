from PyQt5 import QtCore
from pyautogui import screenshot
from datetime import datetime


class ScreenThread(QtCore.QThread):
    def __init__(self):
        super().__init__()
        self.running = False

    def run(self):
        self.running = True
        while self.running:
            self.image = screenshot(f"screen.png")
            self.sleep(120)
