from datetime import datetime
import ntplib


def get_datetime():
    try:
        time_object = ntplib.NTPClient()
        response = time_object.request('2.ru.pool.ntp.org',  timeout=2)
    except ntplib.NTPException:
        return datetime.now().replace(microsecond=0)
    else:
        return datetime.fromtimestamp(response.tx_time).replace(microsecond=0)
