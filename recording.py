from datetime import timedelta
from PyQt5 import QtCore
from get_time import get_datetime


class Record:
    def __init__(self):
        self.time_start = QtCore.QDateTime.currentDateTime()
        self.end_time = QtCore.QDateTime.currentDateTime()
        self.session_cost = 0
        self.half_hour = timedelta(minutes=30)
        self.settings = QtCore.QSettings('Records', 'sound')
        self.session_cost = 1000

    def countdown_begins(self):
        self.start_time = get_datetime()

    def countdown_ends(self):
        self.end_time = get_datetime()
        self.counting_total_cost()

    def counting_total_cost(self):
        self.duration = self.end_time - self.start_time
        print(self.duration)
        if self.duration <= self.half_hour:
            self.session_amount = self.session_cost / 2
        else:
            self.session_amount = self.duration.seconds * self.session_cost / 3600
            self.session_amount = round(self.session_amount * 2, -2) // 2
        self.data_to_string()

    def data_to_string(self):
        self.hours = self.duration.seconds // 3600
        self.minutes = (self.duration.seconds // 60) % 60
        self.seconds = self.duration.seconds
        print(self.hours, self.minutes)
