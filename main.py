import sys
from app import AppWindow
from PyQt5 import QtWidgets, QtCore


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = AppWindow()
    window.show()
    sys.exit(app.exec())


if __name__ == "__main__":
    main()
