from PyQt5 import QtWidgets, QtCore
from recording import Record
from screenshot import ScreenThread
from network import GetToken, SendRecData
import time


class AppWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.rec_actions = Record()
        self.time = QtCore.QTime(0, 0)
        self.timer_id = None
        self.my_thread = ScreenThread()
        self.my_thread.start()
        self.central_widget = QtWidgets.QWidget()
        self.setCentralWidget(self.central_widget)
        self.token_access = None
        self.token_refresh = None
        self.createUI()

        self.add_button_actions()
        if not self.rec_actions.session_cost:
            self.set_start_settings()

    def createUI(self):
        self.button_start = QtWidgets.QPushButton("Старт")
        self.label_time = QtWidgets.QLabel(f"{self.time.toString('hh:mm:ss')}")
        self.label_info = QtWidgets.QLabel(f'Стоимость часа записи: {self.rec_actions.session_cost}')
        self.label_time.setAlignment(QtCore.Qt.AlignCenter)
        self.label_info.setAlignment(QtCore.Qt.AlignCenter)
        self.button_stop = QtWidgets.QPushButton('Стоп')
        self.button_gettoken = QtWidgets.QPushButton('Токен')
        self.vbox = QtWidgets.QVBoxLayout()
        self.vbox.addWidget(self.label_info)
        self.vbox.addWidget(self.label_time)
        self.vbox.addWidget(self.button_start)
        self.vbox.addWidget(self.button_stop)
        self.vbox.addWidget(self.button_gettoken)
        self.central_widget.setLayout(self.vbox)

    def add_button_actions(self):
        self.button_start.clicked.connect(self.start)
        self.button_stop.clicked.connect(self.stop)
        self.button_gettoken.clicked.connect(self.get_token)

    def get_token(self):
        self.auth = GetToken(username='kap&',
                           password='61293')
        self.auth.downloaded.connect(self.token_loader)
        self.auth.doRequest()

    def token_loader(self):
        token = self.ex.downloaded_data()
        self.token_access = token['access']
        self.token_refresh = token['refresh']
        print(self.token_access, self.token_refresh, sep='\n\n')

    def timerEvent(self, event):
        self.time = self.time.addSecs(1)
        self.label_time.setText(f"{self.time.toString('hh:mm:ss')}")

    @QtCore.pyqtSlot(name='start')
    def start_recording(self):
        print(self.token_access)
        self.rec_actions.countdown_begins()
        if not self.timer_id:
            self.timer_id = self.startTimer(1000)

    @QtCore.pyqtSlot(name='stop')
    def end_recording(self):
        self.rec_actions.countdown_ends()
        print(self.rec_actions.start_time,
              self.rec_actions.end_time,
              self.rec_actions.duration,
              self.rec_actions.session_amount,
              self.rec_actions.session_cost)
        if self.timer_id:
            self.killTimer(self.timer_id)
            self.timer_id = None
            self.time = QtCore.QTime(0, 0)
            self.label_time.setText(self.time.toString())
            self.label_info.setText(f"Стоимость записи составила:"
                                    f"{self.rec_actions.session_amount}")
        self.send_record_data = SendRecData(studio='ShowtimeUFA',
                                            token=self.token_access,
                                            start=self.rec_actions.start_time,
                                            end=self.rec_actions.end_time,
                                            duration=self.rec_actions.duration,
                                            cost=self.rec_actions.session_cost,
                                            session_cost=self.rec_actions.session_amount)
        self.send_record_data.rec_downloaded.connect(self.do_send)
        self.send_record_data.do_request()

    def do_send(self):
        print(self.send_record_data.downloaded_info())

    def set_start_settings(self):
        self.rec_actions.cost, ok = QtWidgets.QInputDialog.getInt(self,
                                                                  'Стоимость часа записи',
                                                                  'Выберите стоимость',
                                                                  0, 0, 10000, 100)
        if ok:
            self.rec_actions.settings = self.rec_actions.session_cost
            self.label_info.setText(f'Стоимость часа записи: {self.rec_actions.session_cost}')
