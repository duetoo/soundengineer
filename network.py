import json
from PyQt5 import QtNetwork, QtCore


class GetToken(QtCore.QObject):

    downloaded = QtCore.pyqtSignal()

    def __init__(self, username, password):
        super(GetToken, self).__init__()
        self.username = username
        self.password = password
        self.url = 'http://localhost:8000/api/token/'

    def doRequest(self):
        data = QtCore.QByteArray()
        data.append(f'username={self.username}')
        data.append(f'password={self.password}')
        print(data)
        req = QtNetwork.QNetworkRequest(QtCore.QUrl(self.url))
        req.setHeader(QtNetwork.QNetworkRequest.ContentTypeHeader,
                      'application/x-www-form-urlencoded')

        self.manager = QtNetwork.QNetworkAccessManager()

        self.manager.finished.connect(self.handleResponse)
        self.manager.post(req, data)

    def handleResponse(self, reply):
        er = reply.error()
        if er == QtNetwork.QNetworkReply.NoError:
            self._token_pair = json.loads(str(reply.readAll(), 'utf-8'))
        else:
            print('Error occurred: ', er)
            print(reply.errorString())
        self.downloaded.emit()

    def downloaded_data(self):
        return self._token_pair


class SendRecData(QtCore.QObject):

    rec_downloaded = QtCore.pyqtSignal()

    def __init__(self, studio, token, start, end, duration, session_cost, cost):
        super(SendRecData, self).__init__()
        self._token = token
        self._studio = studio
        self._start = start
        self._end = end
        self._duration = duration
        self._cost = cost
        self._session_cost = session_cost
        self.url = 'http://127.0.0.1:8000/api/v1/records/'

    def do_request(self):
        data = QtCore.QByteArray()
        data.append(f'studio.name={self._studio}&')
        data.append(f'start_recording={self._start}&')
        data.append(f'end_recording={self._end}&')
        data.append(f'duration={self._duration}&')
        data.append(f'cost={self._cost}&')
        data.append(f'session_cost={self._session_cost}')
        req = QtNetwork.QNetworkRequest(QtCore.QUrl(self.url))
        req.setHeader(QtNetwork.QNetworkRequest.ContentTypeHeader,
                      'application/x-www-form-urlencoded')
        req.setRawHeader(b"Authorization", f"Bearer {self._token}".encode())
        self.manager = QtNetwork.QNetworkAccessManager()

        self.manager.finished.connect(self.handle_response)
        self.manager.post(req, data)

    def handle_response(self, reply):
        er = reply.error()
        if er == QtNetwork.QNetworkReply.NoError:
            print('No ok')
        else:
            print('Error occurred: ', er)
            print(reply.errorString())
        self.rec_downloaded.emit()
